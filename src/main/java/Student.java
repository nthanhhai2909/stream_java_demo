import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class Student {
    private String name;
    private boolean isMale; // true - male or false female
    private int age;
    private int score;
    private List<String> course;

    public void print() {
        System.out.println(this.name + " - " + (isMale ? "Male" : "Female") + " - "+ this.age + " - " + this.score);
    }
}
