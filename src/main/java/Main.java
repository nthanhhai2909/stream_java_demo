import com.google.common.collect.Lists;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {

    // Create list student for example
    public static List<Student> students = Lists.newArrayList(
            new Student("A", true, 18, 5, Lists.newArrayList("Maths", "IT", "English")),
            new Student("B", false, 15, 8, Lists.newArrayList("History", "Science", "Art", "English")),
            new Student("C", false, 12, 9, Lists.newArrayList("Spanish", "Geography", "Music")),
            new Student("D", true, 10, 3, Lists.newArrayList("Maths", "IT")),
            new Student("E", true, 10, 2, Lists.newArrayList("Music", "Geometry", "chemistry", "literature")),
            new Student("F", false, 18, 10, Lists.newArrayList("Algebra", "Geometry"))
    );

    public static void demoForEach() {
        students.stream().forEach(s -> System.out.println(s.getName() + " " + s.getAge()));
    }

    // Filter students have age >= 18 and score >= 5
    public static void demoFilter() {
        students.stream().filter(s -> s.getAge() >= 18 && s.getScore() >= 5)
                .forEach(s -> s.print());
    }

    public static void demoCollect() {
        List<Student> listStudent = students.stream().filter(s -> s.getAge() >= 18 && s.getScore() >= 5)
                .collect(Collectors.toList());

        Set<Student> setStudent = students.stream().filter(s -> s.getAge() >= 18 && s.getScore() >= 5)
                .collect(Collectors.toSet());
    }

    // Get list name of students from students
    public static void demoMap() {
        List<String> names = students.stream().map(Student::getName).collect(Collectors.toList());
    }

    // Get all course of students. Data can duplicate
    public static void demoFlatMap() {
        List<String> courses = students.stream().flatMap(s -> s.getCourse().stream()).collect(Collectors.toList());
        for (String cours : courses) {
            System.out.print(cours + " ");
        }
    }

    // Get list course of student. Data must distinct
    public static void demoDistinct() {
        List<String> courses = students.stream().flatMap(s -> s.getCourse().stream())
                .distinct()
                .collect(Collectors.toList());
        for (String cours : courses) {
            System.out.print(cours + " ");
        }
    }

    // Sorted by age
    public static void demoSorted() {
        // Sort age by asc
        students.stream().sorted(Comparator.comparingInt(Student::getAge)).forEach(s -> System.out.println(s.getAge()));
        System.out.println("----------");
        // Sort age by desc
        students.stream().sorted((o1, o2) -> o2.getAge() - o1.getAge()).forEach(s -> System.out.println(s.getAge()));
    }

    public static void demoCount() {
        System.out.println(students.stream().count());
    }

    public static void demoAllMatch() {
        System.out.println(students.stream().allMatch(s -> s.getAge() >= 10));
        System.out.println(students.stream().allMatch(s -> s.getAge() > 10));
    }

    public static void demoAnyMatch() {
        System.out.println(students.stream().anyMatch(s -> s.getAge() > 10));
        System.out.println(students.stream().anyMatch(s -> s.getAge() > 30));
    }

    public static void demoMin() {
        System.out.println(students.stream().min(Comparator.comparing(Student::getAge)).orElse(null).getName());
    }

    public static void demoMax() {
        System.out.println(students.stream().max(Comparator.comparing(Student::getAge)).orElse(null).getName());
    }

    public static void main(String[] args) {
        demoFilter();
        demoMap();
        demoFlatMap();
        demoDistinct();
        demoSorted();
        demoCount();
        demoAllMatch();
        demoMin();
        demoMax();
    }

}
